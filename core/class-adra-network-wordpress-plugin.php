<?php

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * HELPER COMMENT START
 * 
 * This is the main class that is responsible for registering
 * the core functions, including the files and setting up all features. 
 * 
 * To add a new class, here's what you need to do: 
 * 1. Add your new class within the following folder: core/includes/classes
 * 2. Create a new variable you want to assign the class to (as e.g. public $helpers)
 * 3. Assign the class within the instance() function ( as e.g. self::$instance->helpers = new Adra_Network_Wordpress_Plugin_Helpers();)
 * 4. Register the class you added to core/includes/classes within the includes() function
 * 
 * HELPER COMMENT END
 */

if ( ! class_exists( 'Adra_Network_Wordpress_Plugin' ) ) :

	/**
	 * Main Adra_Network_Wordpress_Plugin Class.
	 *
	 * @package		ADRAPLUGIN
	 * @subpackage	Classes/Adra_Network_Wordpress_Plugin
	 * @since		1.0.0
	 * @author		Werner Le Roux
	 */
	final class Adra_Network_Wordpress_Plugin {

		/**
		 * The real instance
		 *
		 * @access	private
		 * @since	1.0.0
		 * @var		object|Adra_Network_Wordpress_Plugin
		 */
		private static $instance;

		/**
		 * ADRAPLUGIN helpers object.
		 *
		 * @access	public
		 * @since	1.0.0
		 * @var		object|Adra_Network_Wordpress_Plugin_Helpers
		 */
		public $helpers;

		/**
		 * ADRAPLUGIN settings object.
		 *
		 * @access	public
		 * @since	1.0.0
		 * @var		object|Adra_Network_Wordpress_Plugin_Settings
		 */
		public $settings;

		/**
		 * Throw error on object clone.
		 *
		 * Cloning instances of the class is forbidden.
		 *
		 * @access	public
		 * @since	1.0.0
		 * @return	void
		 */
		public function __clone() {
			_doing_it_wrong( __FUNCTION__, __( 'You are not allowed to clone this class.', 'adra-network-wordpress-plugin' ), '1.0.0' );
		}

		/**
		 * Disable unserializing of the class.
		 *
		 * @access	public
		 * @since	1.0.0
		 * @return	void
		 */
		public function __wakeup() {
			_doing_it_wrong( __FUNCTION__, __( 'You are not allowed to unserialize this class.', 'adra-network-wordpress-plugin' ), '1.0.0' );
		}

		/**
		 * Main Adra_Network_Wordpress_Plugin Instance.
		 *
		 * Insures that only one instance of Adra_Network_Wordpress_Plugin exists in memory at any one
		 * time. Also prevents needing to define globals all over the place.
		 *
		 * @access		public
		 * @since		1.0.0
		 * @static
		 * @return		object|Adra_Network_Wordpress_Plugin	The one true Adra_Network_Wordpress_Plugin
		 */
		public static function instance() {
			if ( ! isset( self::$instance ) && ! ( self::$instance instanceof Adra_Network_Wordpress_Plugin ) ) {
				self::$instance					= new Adra_Network_Wordpress_Plugin;
				self::$instance->base_hooks();
				self::$instance->includes();
				self::$instance->helpers		= new Adra_Network_Wordpress_Plugin_Helpers();
				self::$instance->settings		= new Adra_Network_Wordpress_Plugin_Settings();

				//Fire the plugin logic
				new Adra_Network_Wordpress_Plugin_Run();

				/**
				 * Fire a custom action to allow dependencies
				 * after the successful plugin setup
				 */
				do_action( 'ADRAPLUGIN/plugin_loaded' );
			}

			return self::$instance;
		}

		/**
		 * Include required files.
		 *
		 * @access  private
		 * @since   1.0.0
		 * @return  void
		 */
		private function includes() {
			require_once ADRAPLUGIN_PLUGIN_DIR . 'core/includes/classes/class-adra-network-wordpress-plugin-helpers.php';
			require_once ADRAPLUGIN_PLUGIN_DIR . 'core/includes/classes/class-adra-network-wordpress-plugin-settings.php';

			require_once ADRAPLUGIN_PLUGIN_DIR . 'core/includes/classes/class-adra-network-wordpress-plugin-run.php';
		}

		/**
		 * Add base hooks for the core functionality
		 *
		 * @access  private
		 * @since   1.0.0
		 * @return  void
		 */
		private function base_hooks() {
			add_action( 'plugins_loaded', array( self::$instance, 'load_textdomain' ) );
		}

		/**
		 * Loads the plugin language files.
		 *
		 * @access  public
		 * @since   1.0.0
		 * @return  void
		 */
		public function load_textdomain() {
			load_plugin_textdomain( 'adra-network-wordpress-plugin', FALSE, dirname( plugin_basename( ADRAPLUGIN_PLUGIN_FILE ) ) . '/languages/' );
		}

	}

endif; // End if class_exists check.