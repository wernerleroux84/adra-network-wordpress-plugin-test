<?php

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * HELPER COMMENT START
 * 
 * This class is used to bring your plugin to life. 
 * All the other registered classed bring features which are
 * controlled and managed by this class.
 * 
 * Within the add_hooks() function, you can register all of 
 * your WordPress related actions and filters as followed:
 * 
 * add_action( 'my_action_hook_to_call', array( $this, 'the_action_hook_callback', 10, 1 ) );
 * or
 * add_filter( 'my_filter_hook_to_call', array( $this, 'the_filter_hook_callback', 10, 1 ) );
 * or
 * add_shortcode( 'my_shortcode_tag', array( $this, 'the_shortcode_callback', 10 ) );
 * 
 * Once added, you can create the callback function, within this class, as followed: 
 * 
 * public function the_action_hook_callback( $some_variable ){}
 * or
 * public function the_filter_hook_callback( $some_variable ){}
 * or
 * public function the_shortcode_callback( $attributes = array(), $content = '' ){}
 * 
 * 
 * HELPER COMMENT END
 */

/**
 * Class Adra_Network_Wordpress_Plugin_Run
 *
 * Thats where we bring the plugin to life
 *
 * @package		ADRAPLUGIN
 * @subpackage	Classes/Adra_Network_Wordpress_Plugin_Run
 * @author		Werner Le Roux
 * @since		1.0.0
 */
class Adra_Network_Wordpress_Plugin_Run{

	/**
	 * Our Adra_Network_Wordpress_Plugin_Run constructor 
	 * to run the plugin logic.
	 *
	 * @since 1.0.0
	 */
	function __construct(){
		$this->add_hooks();
	}

	/**
	 * ######################
	 * ###
	 * #### WORDPRESS HOOKS
	 * ###
	 * ######################
	 */

	/**
	 * Registers all WordPress and plugin related hooks
	 *
	 * @access	private
	 * @since	1.0.0
	 * @return	void
	 */
	private function add_hooks(){
	
		add_action( 'plugin_action_links_' . ADRAPLUGIN_PLUGIN_BASE, array( $this, 'add_plugin_action_link' ), 20 );
		add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_backend_scripts_and_styles' ), 20 );
		add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_frontend_scripts_and_styles' ), 20 );
		add_action( 'wp_ajax_nopriv_my_demo_ajax_call', array( $this, 'my_demo_ajax_call_callback' ), 20 );
		add_action( 'wp_ajax_my_demo_ajax_call', array( $this, 'my_demo_ajax_call_callback' ), 20 );
		add_action( 'heartbeat_nopriv_received', array( $this, 'myplugin_receive_heartbeat' ), 20, 2 );
		add_action( 'heartbeat_received', array( $this, 'myplugin_receive_heartbeat' ), 20, 2 );
		add_action( 'admin_bar_menu', array( $this, 'add_admin_bar_menu_items' ), 100, 1 );
		add_filter( 'edd_settings_sections_extensions', array( $this, 'add_edd_settings_section' ), 20 );
		add_filter( 'edd_settings_extensions', array( $this, 'add_edd_settings_section_content' ), 20 );
		add_action( 'plugins_loaded', array( $this, 'add_wp_webhooks_integrations' ), 9 );
		add_filter( 'wpwhpro/admin/settings/menu_data', array( $this, 'add_main_settings_tabs' ), 20 );
		add_action( 'wpwhpro/admin/settings/menu/place_content', array( $this, 'add_main_settings_content' ), 20 );
	
	}

	/**
	 * ######################
	 * ###
	 * #### WORDPRESS HOOK CALLBACKS
	 * ###
	 * ######################
	 */

	/**
	* Adds action links to the plugin list table
	*
	* @access	public
	* @since	1.0.0
	*
	* @param	array	$links An array of plugin action links.
	*
	* @return	array	An array of plugin action links.
	*/
	public function add_plugin_action_link( $links ) {

		$links['our_shop'] = sprintf( '<a href="%s" target="_blank title="Adra Network WordPress Plugin" style="font-weight:700;">%s</a>', 'https://gitlab.com/wernerleroux84', __( 'Adra Network WordPress Plugin', 'adra-network-wordpress-plugin' ) );

		return $links;
	}

	/**
	 * Enqueue the backend related scripts and styles for this plugin.
	 * All of the added scripts andstyles will be available on every page within the backend.
	 *
	 * @access	public
	 * @since	1.0.0
	 *
	 * @return	void
	 */
	public function enqueue_backend_scripts_and_styles() {
		wp_enqueue_style( 'adraplugin-backend-styles', ADRAPLUGIN_PLUGIN_URL . 'core/includes/assets/css/backend-styles.css', array(), ADRAPLUGIN_VERSION, 'all' );

		if( ! wp_script_is( 'heartbeat' ) ){
			//enqueue the Heartbeat API
			wp_enqueue_script( 'heartbeat' );
		}

		wp_enqueue_script( 'adraplugin-backend-scripts', ADRAPLUGIN_PLUGIN_URL . 'core/includes/assets/js/backend-scripts.js', array( 'jquery' ), ADRAPLUGIN_VERSION, true );
		wp_localize_script( 'adraplugin-backend-scripts', 'adraplugin', array(
			'plugin_name'   	=> __( ADRAPLUGIN_NAME, 'adra-network-wordpress-plugin' ),
			'ajaxurl' 			=> admin_url( 'admin-ajax.php' ),
			'security_nonce'	=> wp_create_nonce( "your-nonce-name" ),
		));
	}


	/**
	 * Enqueue the frontend related scripts and styles for this plugin.
	 *
	 * @access	public
	 * @since	1.0.0
	 *
	 * @return	void
	 */
	public function enqueue_frontend_scripts_and_styles() {
		wp_enqueue_style( 'adraplugin-frontend-styles', ADRAPLUGIN_PLUGIN_URL . 'core/includes/assets/css/frontend-styles.css', array(), ADRAPLUGIN_VERSION, 'all' );

		if( ! wp_script_is( 'heartbeat' ) ){
			//enqueue the Heartbeat API
			wp_enqueue_script( 'heartbeat' );
		}

		wp_enqueue_script( 'adraplugin-frontend-scripts', ADRAPLUGIN_PLUGIN_URL . 'core/includes/assets/js/frontend-scripts.js', array( 'jquery' ), ADRAPLUGIN_VERSION, true );
		wp_localize_script( 'adraplugin-frontend-scripts', 'adraplugin', array(
			'demo_var'   		=> __( 'This is some demo text coming from the backend through a variable within javascript.', 'adra-network-wordpress-plugin' ),
			'ajaxurl' 			=> admin_url( 'admin-ajax.php' ),
			'security_nonce'	=> wp_create_nonce( "your-nonce-name" ),
		));
	}


	/**
	 * The callback function for my_demo_ajax_call
	 *
	 * @access	public
	 * @since	1.0.0
	 *
	 * @return	void
	 */
	public function my_demo_ajax_call_callback() {
		check_ajax_referer( 'your-nonce-name', 'ajax_nonce_parameter' );

		$demo_data = isset( $_REQUEST['demo_data'] ) ? sanitize_text_field( $_REQUEST['demo_data'] ) : '';
		$response = array( 'success' => false );

		if ( ! empty( $demo_data ) ) {
			$response['success'] = true;
			$response['msg'] = __( 'The value was successfully filled.', 'adra-network-wordpress-plugin' );
		} else {
			$response['msg'] = __( 'The sent value was empty.', 'adra-network-wordpress-plugin' );
		}

		if( $response['success'] ){
			wp_send_json_success( $response );
		} else {
			wp_send_json_error( $response );
		}

		die();
	}


	/**
	 * The callback function for heartbeat_received
	 *
	 * @access	public
	 * @since	1.0.0
	 *
	 * @param	array	$response	Heartbeat response data to pass back to front end.
	 * @param	array	$data		Data received from the front end (unslashed).
	 *
	 * @return	array	$response	The adjusted heartbeat response data
	 */
	public function myplugin_receive_heartbeat( $response, $data ) {

		//If we didn't receive our data, don't send any back.
		if( empty( $data['myplugin_customfield'] ) ){
			return $response;
		}

		// Calculate our data and pass it back. For this example, we'll hash it.
		$received_data = $data['myplugin_customfield'];

		$response['myplugin_customfield_hashed'] = sha1( $received_data );

		return $response;
	}

	/**
	 * Add a new menu item to the WordPress topbar
	 *
	 * @access	public
	 * @since	1.0.0
	 *
	 * @param	object $admin_bar The WP_Admin_Bar object
	 *
	 * @return	void
	 */
	public function add_admin_bar_menu_items( $admin_bar ) {

		$admin_bar->add_menu( array(
			'id'		=> 'adra-network-wordpress-plugin-id', // The ID of the node.
			'title'		=> __( 'Adra Network Plugin', 'adra-network-wordpress-plugin' ), // The text that will be visible in the Toolbar. Including html tags is allowed.
			'parent'	=> false, // The ID of the parent node.
			'href'		=> '#', // The ‘href’ attribute for the link. If ‘href’ is not set the node will be a text node.
			'group'		=> false, // This will make the node a group (node) if set to ‘true’. Group nodes are not visible in the Toolbar, but nodes added to it are.
			'meta'		=> array(
				'title'		=> __( 'Adra Network Plugin', 'adra-network-wordpress-plugin' ), // The title attribute. Will be set to the link or to a div containing a text node.
				'target'	=> '_blank', // The target attribute for the link. This will only be set if the ‘href’ argument is present.
				'class'		=> 'adra-network-wordpress-plugin-class', // The class attribute for the list item containing the link or text node.
				'html'		=> false, // The html used for the node.
				'rel'		=> false, // The rel attribute.
				'onclick'	=> false, // The onclick attribute for the link. This will only be set if the ‘href’ argument is present.
				'tabindex'	=> false, // The tabindex attribute. Will be set to the link or to a div containing a text node.
			),
		));

		$admin_bar->add_menu( array(
			'id'		=> 'adra-network-wordpress-plugin-sub-id',
			'title'		=> __( 'Plugin Options', 'adra-network-wordpress-plugin' ),
			'parent'	=> 'adra-network-wordpress-plugin-id',
			'href'		=> '#',
			'group'		=> false,
			'meta'		=> array(
				'title'		=> __( 'Plugin Options', 'adra-network-wordpress-plugin' ),
				'target'	=> '_blank',
				'class'		=> 'adra-network-wordpress-plugin-sub-class',
				'html'		=> false,    
				'rel'		=> false,
				'onclick'	=> false,
				'tabindex'	=> false,
			),
		));

	}

	/**
	 * Add the custom settings section under
	 * Downloads -> Settings -> Extensions
	 *
	 * @access	public
	 * @since	1.0.0
	 *
	 * @param	array	$sections	The currently registered EDD settings sections
	 *
	 * @return	void
	 */
	public function add_edd_settings_section( $sections ) {
		
		$sections['adra-network-wordpress-plugin'] = __( ADRAPLUGIN()->settings->get_plugin_name(), 'adra-network-wordpress-plugin' );

		return $sections;
	}

	/**
	 * Add the custom settings section content
	 *
	 * @access	public
	 * @since	1.0.0
	 *
	 * @param	array	$settings	The currently registered EDD settings for all registered extensions
	 *
	 * @return	array	The extended settings 
	 */
	public function add_edd_settings_section_content( $settings ) {
		
		// Your settings reamain registered as they were in EDD Pre-2.5
		$custom_settings = array(
			array(
				'id'   => 'my_header',
				'name' => '<strong>' . __( ADRAPLUGIN()->settings->get_plugin_name() . 'Settings', 'adra-network-wordpress-plugin' ) . '</strong>',
				'desc' => '',
				'type' => 'header',
				'size' => 'regular'
			),
			array(
				'id'    => 'my_example_setting',
				'name'  => __( 'Example checkbox', 'adra-network-wordpress-plugin' ),
				'desc'  => __( 'Check this to turn on a setting', 'adra-network-wordpress-plugin' ),
				'type'  => 'checkbox'
			),
			array(
				'id'    => 'my_example_text',
				'name'  => __( 'Example text', 'adra-network-wordpress-plugin' ),
				'desc'  => __( 'A Text setting', 'adra-network-wordpress-plugin' ),
				'type'  => 'text',
				'std'   => __( 'Example default text', 'adra-network-wordpress-plugin' )
			),
		);

		// If EDD is at version 2.5 or later...
		if ( version_compare( EDD_VERSION, 2.5, '>=' ) ) {
			$custom_settings = array( 'adra-network-wordpress-plugin' => $custom_settings );
		}

		return array_merge( $settings, $custom_settings );
	}

	/**
	 * ####################
	 * ### WP Webhooks 
	 * ####################
	 */

	/*
	 * Register dynamically all integrations
	 * The integrations are available within core/includes/integrations.
	 * A new folder is considered a new integration.
	 *
	 * @access	public
	 * @since	1.0.0
	 *
	 * @return	void
	 */
	public function add_wp_webhooks_integrations(){

		// Abort if WP Webhooks is not active
		if( ! function_exists('WPWHPRO') ){
			return;
		}

		$custom_integrations = array();
		$folder = ADRAPLUGIN_PLUGIN_DIR . 'core' . DIRECTORY_SEPARATOR . 'includes' . DIRECTORY_SEPARATOR . 'integrations';

		try {
			$custom_integrations = WPWHPRO()->helpers->get_folders( $folder );
		} catch ( Exception $e ) {
			WPWHPRO()->helpers->log_issue( $e->getTraceAsString() );
		}

		if( ! empty( $custom_integrations ) ){
			foreach( $custom_integrations as $integration ){
				$file_path = $folder . DIRECTORY_SEPARATOR . $integration . DIRECTORY_SEPARATOR . $integration . '.php';
				WPWHPRO()->integrations->register_integration( array(
					'slug' => $integration,
					'path' => $file_path,
				) );
			}
		}
	}

	/*
	 * Add the setting tabs
	 *
	 * @access	public
	 * @since	1.0.0
	 *
	 * @param	mixed	$tabs	All available tabs
	 *
	 * @return	array	$data
	 */
	public function add_main_settings_tabs( $tabs ){

		$tabs['demo'] = WPWHPRO()->helpers->translate( 'Demo', 'admin-menu' );

		return $tabs;

	}

	/*
	 * Output the content of the tab
	 *
	 * @access	public
	 * @since	1.0.0
	 *
	 * @param	mixed	$tab	The current tab
	 *
	 * @return	void
	 */
	public function add_main_settings_content( $tab ){

		switch($tab){
			case 'demo':
				echo '<div class="wpwh-container">This is some custom text for our very own demo tab.</div>';
				break;
		}

	}

}
