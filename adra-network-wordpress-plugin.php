<?php
/**
 * Adra Network WordPress Plugin
 *
 * @package       ADRAPLUGIN
 * @author        Werner Le Roux
 * @license       gplv3-or-later
 * @version       1.0.0
 *
 * @wordpress-plugin
 * Plugin Name:   Adra Network WordPress Plugin
 * Plugin URI:    https://gitlab.com/wernerleroux84
 * Description:   Adra Network WordPress Plugin are developed with UX/UI best practices in mind. The end product will be user friendly and easy to use.
 * Version:       1.0.0
 * Author:        Werner Le Roux
 * Author URI:    https://gitlab.com/wernerleroux84
 * Text Domain:   adra-network-wordpress-plugin
 * Domain Path:   /languages
 * License:       GPLv3 or later
 * License URI:   https://www.gnu.org/licenses/gpl-3.0.html
 *
 * You should have received a copy of the GNU General Public License
 * along with Adra Network WordPress Plugin. If not, see <https://www.gnu.org/licenses/gpl-3.0.html/>.
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * HELPER COMMENT START
 * 
 * This file contains the main information about the plugin.
 * It is used to register all components necessary to run the plugin.
 * 
 * The comment above contains all information about the plugin 
 * that are used by WordPress to differenciate the plugin and register it properly.
 * It also contains further PHPDocs parameter for a better documentation
 * 
 * The function ADRAPLUGIN() is the main function that you will be able to 
 * use throughout your plugin to extend the logic. Further information
 * about that is available within the sub classes.
 * 
 * HELPER COMMENT END
 */

// Plugin name
define( 'ADRAPLUGIN_NAME',			'Adra Network WordPress Plugin' );

// Plugin version
define( 'ADRAPLUGIN_VERSION',		'1.0.0' );

// Plugin Root File
define( 'ADRAPLUGIN_PLUGIN_FILE',	__FILE__ );

// Plugin base
define( 'ADRAPLUGIN_PLUGIN_BASE',	plugin_basename( ADRAPLUGIN_PLUGIN_FILE ) );

// Plugin Folder Path
define( 'ADRAPLUGIN_PLUGIN_DIR',	plugin_dir_path( ADRAPLUGIN_PLUGIN_FILE ) );

// Plugin Folder URL
define( 'ADRAPLUGIN_PLUGIN_URL',	plugin_dir_url( ADRAPLUGIN_PLUGIN_FILE ) );

/**
 * Load the main class for the core functionality
 */
require_once ADRAPLUGIN_PLUGIN_DIR . 'core/class-adra-network-wordpress-plugin.php';

/**
 * The main function to load the only instance
 * of our master class.
 *
 * @author  Werner Le Roux
 * @since   1.0.0
 * @return  object|Adra_Network_Wordpress_Plugin
 */
function ADRAPLUGIN() {
	return Adra_Network_Wordpress_Plugin::instance();
}

ADRAPLUGIN();
