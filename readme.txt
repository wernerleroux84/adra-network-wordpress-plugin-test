=== Adra Network WordPress Plugin ===
Author URI: https://gitlab.com/wernerleroux84
Plugin URI: https://gitlab.com/wernerleroux84
Donate link: 
Contributors: Werner Le Roux, Francois Auclaire
Tags: adra-plugin
Requires at least: 6.1
Tested up to: 6.0
Requires PHP: 8.1
Stable tag: 1.0.0
License: GPLv3 or later
License URI: https://www.gnu.org/licenses/gpl-3.0.html

Adra Network WordPress Plugin are developed with UX/UI best practices in mind. The end product will be user friendly and easy to use.

== Description ==

Adra Network WordPress Plugin

== Frequently Asked Questions ==

= A question that someone might have =

An answer to that question.


== Installation ==

1. Go to `Plugins` in the Admin menu
2. Click on the button `Add new`
3. Search for `Adra Network WordPress Plugin` and click 'Install Now' or click on the `upload` link to upload `adra-network-wordpress-plugin.zip`
4. Click on `Activate plugin`

== Changelog ==

= 1.0.0: December 25, 2022 =
* Birthday of Adra Network WordPress Plugin