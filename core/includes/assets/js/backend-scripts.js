/*------------------------ 
Backend related javascript
------------------------*/

/**
 * HELPER COMMENT START
 * 
 * This file contains all of the backend related javascript. 
 * With backend, it is meant the WordPress admin area.
 * 
 * Since you added the jQuery dependency within the "Add JS support" module, you see down below
 * the helper comment a function that allows you to use jQuery with the commonly known notation: $('')
 * By default, this notation is deactivated since WordPress uses the noConflict mode of jQuery
 * You can also use jQuery outside using the following notation: jQuery('')
 * 
 * Here's some jQuery example code you can use to fire code once the page is loaded: $(document).ready( function(){} );
 * 
 * Using the ajax example, you can send data back and forth between your frontend and the 
 * backend of the website (PHP to ajax and vice-versa). 
 * As seen in the example below, we use the jQuery $.ajax function to send data to the WordPress
 * callback my_demo_ajax_call, which was added within the Adra_Network_Wordpress_Plugin_Run class.
 * From there, we process the data and send it back to the code below, which will then display the 
 * example within the console of your browser.
 * 
 * As you added the heartbeat example, you will find two new function callbacks down below
 * within the code. The heartbeat-send callback is used to send the data from Javascript to
 * your server.
 * The heartbeat-tick will then receive back the validated data of the callback, based on the 
 * myplugin_receive_heartbeat data within the Adra_Network_Wordpress_Plugin_Run class.
 * 
 * You can add the localized variables in here as followed: adraplugin.plugin_name
 * These variables are defined within the localization function in the following file:
 * core/includes/classes/class-adra-network-wordpress-plugin-run.php
 * 
 * HELPER COMMENT END
 */

(function( $ ) {

	"use strict";

	$(document).ready( function() {
		$.ajax({
			type : "post",
			dataType : "json",
			url : adraplugin.ajaxurl,
			data : {
				action: "my_demo_ajax_call", 
				demo_data : 'test_data', 
				ajax_nonce_parameter: adraplugin.security_nonce
			},
			success: function(response) {
				console.log( response );
			}
		});
	});

	$( document ).on( 'heartbeat-send', function( event, data ){
		// Add additional data to Heartbeat data.
		data.myplugin_customfield = 'some_data';
	});

	$( document ).on( 'heartbeat-tick', function( event, data ){
		// Check for our data, and use it.
		if( ! data.myplugin_customfield_hashed ){
			return;
		}
	
		alert( 'The hash is ' + data.myplugin_customfield_hashed );
	});

})( jQuery );
